﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSoft.EfTest.DataAccess.EFDB
{
    public partial class User_Info
    {

        public enum GenderEnum
        {
            Male=0,
            Female=1,
            Other=2
        }

        public GenderEnum Gender
        {
            get { return (GenderEnum)GenderEnumId; }
            set
            {
                GenderEnumId = (Byte)value;
            }
        }
        //public enum StatusEnum
        //{
        //    Active = 0,
        //    Inactive = 1,

        //}
        //public StatusEnum Status
        //{
        //    get { return (StatusEnum)int.Parse(StatusEnumId.ToString()); }
        //    set
        //    {
        //        StatusEnumId = (Byte)value;
        //    }
        //}
        public string JoinDateString
        {
            get { return JoinData.ToString("dd-MM-yyyy HH:mm"); }
            set
            {
                DateTimeStyles st=DateTimeStyles.None;

                var joinData = JoinData;

                DateTime.TryParseExact(value, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture, st,out joinData);

                JoinData = joinData;
            }
        }

        public static User_Info GetNew()
        {
            User_Info user= new User_Info();
            user.GenderEnumId =(int)GenderEnum.Male;
            user.Name ="";
            user.Email ="";
            user.JoinData=DateTime.Today;
            user.Password ="";
            user.StatusEnumId = false;
            user.CountryId = 1;
            return user;

        }
    }
}
