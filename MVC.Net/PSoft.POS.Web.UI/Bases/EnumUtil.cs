﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSoft.POS.Web.UI.Bases
{

    public static class EnumUtil
    {
        public class EnumInfo
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Type EnumType { get; set; }

        }

        //public static EnumInfo GetEnum(int i, string typeName )
        //{
        //    Type type = ReflectionHelper.GetType(typeName);
        //    if (type == null) return null;
        //    return GetEnum(i, type);
        //}

        public static EnumInfo GetEnum(int i, Type eType)
        {
            return new EnumInfo()
            {
                EnumType = eType,
                Id = i,
                Name = Enum.GetName(eType, i).AddSpacesToSentence()
            };
        }
        //public static EnumInfo GetEnum(object i, string typeName)
        //{
        //    Type type = ReflectionHelper.GetType(typeName);
        //    if (type == null) return null;
        //    return GetEnum(i, type);
        //}
        public static EnumInfo GetEnum(object i, Type eType)
        {
            return new EnumInfo()
            {
                //EnumType = eType,
                Id = Convert.ToInt32(i),
                Name = Enum.GetName(eType, i).AddSpacesToSentence()
            };
        }
        //public static IList<EnumInfo> GetEnumList( string typeName)
        //{
        //    Type type = ReflectionHelper.GetType(typeName);
        //    if (type == null) return null;
        //    return GetEnumList( type);
        //}
        public static IList<EnumInfo> GetEnumList(Type enumType)
        {
            var enums = new List<EnumInfo>();

            if (!enumType.IsEnum)
                return enums;


            {
                enums.AddRange(from object v in Enum.GetValues(enumType)
                               select new EnumInfo()
                               {
                                   //EnumType = enumType,
                                   Id = (int)v,
                                   Name = Enum.GetName(enumType, v).AddSpacesToSentence()
                               });

            }

            return enums;
        }
        public static IList<EnumInfo> GetEnumToUpperList(Type enumType)
        {
            var enums = new List<EnumInfo>();

            if (!enumType.IsEnum)
                return enums;

            enums.AddRange(from object v in Enum.GetValues(enumType)
                           select new EnumInfo()
                           {
                               //EnumType = enumType,
                               Id = (int)v,
                               Name = Enum.GetName(enumType, v).AddSpacesToSentence().ToUpper()
                           });

            return enums;
        }
        public static string AddSpacesToSentence(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return "";
            text = text.Replace("Phd", "PhD");
            text = text.Replace("phd", "PhD");
            StringBuilder newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if ((char.IsUpper(text[i]) || char.IsDigit(text[i]) || text[i] == '_') && text[i - 1] != ' ')
                {
                    newText.Append(' ');
                }
                if (text[i] != '_')
                {
                    newText.Append(text[i]);
                }
            }
            return newText.ToString();
        }

        

    }
}