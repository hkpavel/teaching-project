﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PSoft.EfTest.DataAccess.EFDB;
using PSoft.POS.Web.UI.Bases;
using WebGrease.Activities;

namespace PSoft.POS.Web.UI.Controllers
{
    public class UserAjaxController : BaseController
    {
        public ActionResult Manager()
        {
            // User_Info user=new User_Info();
            // Country 
            return View();
        }
         
        public ActionResult AddEdit(int id=0)
        {

            //todo check validation
            User_Info user=new User_Info();
            if (id>0)
            {
                user = DbContext.User_Info.SingleOrDefault(x => x.Id == id);
            }
            ViewBag.Success = TempData["Success"];
            return View(user);
        }

        private bool IsValidToSaveUser(User_Info newObj, out string error)
        {
            error = "";

            if (!newObj.Name.IsValid())
            {
                error += "Name is not valid.";
                return false;
            }
            if (newObj.Name.Length > 255)
            {
                error += "Name exceeded its maximum length 255.";
                return false;
            }


            if (newObj.CountryId == null)
            {
                error += "Country isnt valid .";
                return false;
            }
            if (newObj.CountryId < 1)
            {
                error += "Country isn't valid.";
                return false;
            }


            return true;
        }
        //[HttpPost]
        //public ActionResult AddEdit(User_Info user)
        //{
        //    try
        //    {
        //        string error = string.Empty;
        //        //int id = Request
        //        //todo check validation
        //        if (!IsValidToSaveUser(user,out error))
        //        {
        //            ViewBag.Error = "Attention please! User saving faild." + error;
        //            TempData["Error"] = "Attention please! User saving faild." + error;
        //            return View();
        //        }
        //        User_Info dbuser = DbContext.User_Info.SingleOrDefault(x => x.Id == user.Id);
        //        //DbContext.User_Info.Add(user);
        //        if (dbuser == null)
        //        {
        //            //create
        //            DbContext.User_Info.Add(user);
        //            DbContext.SaveChanges();
        //            //var url = Url.Action("AddEdit", "User", new { Id = user.Id });
        //            //return Redirect(url);
        //            TempData["Success"] = "User Created Successfully";
        //            //ViewBag.Success = "User Created Successfully";
        //            return RedirectToAction("AddEdit", "User", new { Id = user.Id });
        //        }
        //        else
        //        {
        //            //edit
        //            dbuser.Email = user.Email;
        //            dbuser.CountryId = user.CountryId;
        //            dbuser.GenderEnumId = user.GenderEnumId;
        //            dbuser.Name = user.Name;
        //            DbContext.SaveChanges();
        //            //TempData["Success"] = "User Updated Successfully";
        //            ViewBag.Success = "User Updated Successfully";
        //            ModelState.Clear();
        //            return View(user);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        //LogError()
        //        ViewBag.Error = "Attention please! User creation faild, Try again."+ ex.ToString();
        //        return View();
        //    }

           
        //}

        [HttpPost]
        public JsonResult Save(User_Info user)
        {
            var result = HttpResult.GetNew();
            try
            {
                string error = string.Empty;
                //int id = Request
                //todo check validation
                if (!IsValidToSaveUser(user, out error))
                {
                    result.Data = "" + error;
                    result.HasError = true;
                    return Json(result) ;
                }
                User_Info dbuser = DbContext.User_Info.SingleOrDefault(x => x.Id == user.Id);

                if (dbuser == null)
                {
                    //create
                    DbContext.User_Info.Add(user);
                    DbContext.SaveChanges();
                    result.Data = user.Id;
                    return Json(result);
                }
                else
                {
                    //edit
                    dbuser.Email = user.Email;
                    dbuser.CountryId = user.CountryId;
                    dbuser.GenderEnumId = user.GenderEnumId;
                    dbuser.Name = user.Name;
                    DbContext.SaveChanges();
                    result.Data = user.Id;
                    return Json(result);
                }

            }
            catch (Exception ex)
            {
                //LogError()
                result.Data = "" + ex.ToString();
                result.HasError = true;
                return Json(result);
            }


        }

        public ActionResult List()
        {
            var userList = DbContext.User_Info.Include(x=>x.Country).ToList();
            return View(userList);
        }
        public bool Delete(int id)
        {
            //todo check validation
            if (id > 0)
            {
                EfTestDbContext DbContext = new EfTestDbContext();
                var user = DbContext.User_Info.SingleOrDefault(x => x.Id == id);
                if (user != null)
                {
                    DbContext.User_Info.Remove(user);
                    DbContext.SaveChanges();
                    return true;
                    // TempData["success"] = "User Deleted Successfully";
                }
            }
            return  false;

        }
    }
}