﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using PSoft.EfTest.DataAccess.EFDB;
using PSoft.POS.Web.UI.Bases;
using PSoft.POS.Web.UI.Models;
using WebGrease.Activities;

namespace PSoft.POS.Web.UI.Controllers
{
    public class UserAngularController : BaseController
    {
        
        #region MVC Actions
        public ActionResult Manager()
        {
            return View();
        }
        [ChildActionOnly]
        public ActionResult AddEdit(int id = 0)
        {
           return View();
        }
        [ChildActionOnly]
        public ActionResult List()
        {
           
            return View();
        }
        #endregion

        #region Angular Api
        public string GetUserList()
        {
            var result = HttpResult.GetNew();
            try
            {
                string error = string.Empty;

              

                List<User_Info> userList = DbContext.User_Info.Include(x => x.Country).ToList();//

                List<UserJson> userJsonList = UserJson.ToJson(userList);


                result.Data = userJsonList;
                return ToJson(result);
            }
            catch (Exception ex)
            {
                //LogError()
                result.Errors.Add(ex.ToString()); 
                result.HasError = true;
                return ToJson(result);
            }


        }

        public string GetDataExtra()
        {
            return null;
        }
        public string GetUserById(int id = 0)
        {
            var result = HttpResult.GetNew();
            User_Info user = null;
            if (id > 0)
            {
                user = DbContext.User_Info.SingleOrDefault(x => x.Id == id);
            }
            if (user==null)
            {
                user = User_Info.GetNew();
            }
            result.Data = user;
            result.DataBag.CountryList = DbContext.Country.ToList();
            result.DataBag.GenderEnumList= EnumUtil.GetEnumList(typeof(User_Info.GenderEnum));

            return ToJson(result);
        }
        private bool IsValidToSaveUser(User_Info newObj, out string error)
        {
            error = "";

            if (!newObj.Name.IsValid())
            {
                error += "Name is not valid.";
                return false;
            }
            if (newObj.Name.Length > 255)
            {
                error += "Name exceeded its maximum length 255.";
                return false;
            }


            if (newObj.CountryId == null)
            {
                error += "Country isnt valid .";
                return false;
            }
            if (newObj.CountryId < 1)
            {
                error += "Country isn't valid.";
                return false;
            }


            return true;
        }
        [HttpPost]
        public string Save(User_Info user)
        {
            var result = HttpResult.GetNew();
            try
            {
                string error = string.Empty;
                //int id = Request
                //todo check validation
                if (!IsValidToSaveUser(user, out error))
                {
                    result.Errors.Add(error) ;
                    result.HasError = true;
                    return ToJson(result);
                }
                User_Info dbuser = DbContext.User_Info.SingleOrDefault(x => x.Id == user.Id);

                if (dbuser == null)
                {
                    //create
                   
                    DbContext.User_Info.Add(user);
                    DbContext.SaveChanges();
                    result.Data = user;
                    return ToJson(result);
                }
                else
                {
                    //edit
                    dbuser.Email = user.Email;
                    dbuser.CountryId = user.CountryId;
                    dbuser.GenderEnumId = user.GenderEnumId;
                    dbuser.Name = user.Name;
                    DbContext.SaveChanges();
                    result.Data = user;
                    return ToJson(result);
                }

            }
            catch (Exception ex)
            {
                //LogError()
                result.Errors.Add(ex.ToString());
                result.HasError = true;
                return ToJson(result);
            }


        }
        public string Delete(int id)
        {
            //todo check validation
            var result = HttpResult.GetNew();
         
            result.HasError = true;
            try
            {
                string error = string.Empty;
                if (id > 0)
            {
                EfTestDbContext DbContext = new EfTestDbContext();
                var user = DbContext.User_Info.SingleOrDefault(x => x.Id == id);
                if (user != null)
                {
                    DbContext.User_Info.Remove(user);
                    DbContext.SaveChanges();
                        result.HasError = false;
                        return ToJson(result);
               }
                    else
                    {
                        result.Errors.Add("User Not Found.");
                    }

                }
                else
                {
                    result.Errors.Add("Invalid User.");
                }
                return ToJson(result);
            }
            catch (Exception ex)
            {
                //LogError()
                result.Errors.Add(ex.ToString());
                result.HasError = true;
                return ToJson(result);
    }

}
        #endregion



    }
}