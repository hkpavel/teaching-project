﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using PSoft.POS.Web.UI.Bases;

namespace PSoft.POS.Web.UI
{
    public interface ICoke
    {
        Coke GetNew();

        bool Destroy();

    }
}
