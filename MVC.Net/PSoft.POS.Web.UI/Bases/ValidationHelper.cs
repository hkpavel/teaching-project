﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace PSoft.POS.Web.UI.Bases
{
    public static class ValidationHelper
    {
        public static bool IsValid(this string value)
        {
            if (String.IsNullOrEmpty(value))
                return false;
            else if (String.IsNullOrWhiteSpace(value))
                return false;

            return true;
        }
        public static DateTime SqlMinDateTime
        {
            get
            {

                return DateTime.Parse(SqlDateTime.MinValue.ToString());
            }
        }
        public static DateTime SqlMaxDateTime
        {
            get
            {
                return DateTime.Parse(SqlDateTime.MaxValue.ToString());
            }
        }
        public static bool IsValid(this DateTime value)
        {
            try
            {
                if (value < SqlMinDateTime || value > SqlMaxDateTime)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

    }
}