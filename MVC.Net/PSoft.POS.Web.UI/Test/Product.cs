﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using PSoft.EfTest.DataAccess.EFDB;

namespace PSoft.POS.Web.UI.Bases
{
    public  class Product
    {

        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }

        public Supplier Supplier { get; set; }
        public Object Object { get; set; }
        public virtual User_Info UsetInfo { get; set; }

        public static Product GetNew()
        {
            Product pd= new Product();
            pd.Id = 0;
            pd.Name = "";
            pd.Supplier =new Supplier();
            pd.Supplier.Name = "";

            return pd;


        }

        public  Product GetNew2(int x)
        {
            
            Product pd = new Product();
            pd.Id = 0;
            pd.Name = "";
            pd.Supplier = new Supplier();
            pd.Supplier.Name = "";

            return pd;


        }

    }
}