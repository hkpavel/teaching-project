﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PSoft.POS.Web.UI.Startup))]
namespace PSoft.POS.Web.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
