﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using PSoft.EfTest.DataAccess.EFDB;

namespace PSoft.POS.Web.UI.Bases
{
    public class BaseController : Controller
    {

        public EfTestDbContext DbContext=null;

        public BaseController()
        {
            DbContext = new EfTestDbContext();
            DbContext.Configuration.LazyLoadingEnabled = false;
            DbContext.Configuration.ProxyCreationEnabled = false;

        }
        /// <summary>
        /// To avoid circuler referance error. 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string ToJson(object obj)
        {
            //Json(obj, JsonRequestBehavior.AllowGet);
          return  JsonConvert.SerializeObject(obj, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
        }

        public class HttpResult
        {
            public bool HasError { get; set; }
            public List<string> Errors { get; set; }
            public Object Data { get; set; }
            public dynamic DataBag { get; set; }

            public static HttpResult GetNew()
            {
                return new HttpResult()
                {
                    Data = null,
                    DataBag=new ExpandoObject(),
                    HasError = false,
                    Errors=new List<string>()
                };
            }
        }
    }
}