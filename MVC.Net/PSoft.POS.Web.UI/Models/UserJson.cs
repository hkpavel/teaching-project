﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PSoft.EfTest.DataAccess.EFDB;

namespace PSoft.POS.Web.UI.Models
{
    public class UserJson:User_Info
    {
        public new string Gender { get; set; }

        public  string CountryName { get; set; }
 

        public static User_Info ToEntity(UserJson userJson)
        {
            if (userJson == null)
                   return null;

            //if (toUserEntity==null)
            //{
            //        toUserEntity=new User_Info();
            //}
            User_Info toUserEntity = new User_Info();
            toUserEntity.Id = userJson.Id;
            toUserEntity.Email = userJson.Email;
            toUserEntity.CountryId = userJson.CountryId;
            toUserEntity.GenderEnumId = userJson.GenderEnumId;
            //toUserJson.Password = "";//userEntity.Password;
            //toUserEntity.JoinData = userJson.JoinData;
            toUserEntity.StatusEnumId = userJson.StatusEnumId;
            toUserEntity.JoinDateString = userJson.JoinDateString;
            return toUserEntity;
        }


        public static UserJson ToJson(User_Info userEntity)
        {
            if (userEntity == null)
                return null;

            //if (toUserJson == null)
            //{
            //     toUserJson = new UserJson();
            //}
            UserJson toUserJson = new UserJson();
            toUserJson.Id = userEntity.Id;
            toUserJson.Email = userEntity.Email;
            toUserJson.CountryId = userEntity.CountryId;
            toUserJson.GenderEnumId = userEntity.GenderEnumId;
           // toUserJson.Password = "";//userEntity.Password;
            toUserJson.JoinData = userEntity.JoinData;
            toUserJson.StatusEnumId = userEntity.StatusEnumId;

            //
            toUserJson.Gender = userEntity.Gender.ToString();
            toUserJson.JoinDateString = userEntity.JoinDateString;
            toUserJson.CountryName = "";
            if (userEntity.Country!=null)
            {
                toUserJson.CountryName = userEntity.Country.Name;
            }
          

            return toUserJson;


        }

        public static List<UserJson> ToJson(List<User_Info> userEntity)
        {
            if (userEntity == null)
                return null;

            List<UserJson> userJsonList = new List<UserJson>();


            foreach (var user in userEntity)
            {
                UserJson userJson = UserJson.ToJson(user);
                userJsonList.Add(userJson);
            }

            return userJsonList;
        }


    }
}